The runner is provided as resolution to the specified question.

To install, run:

```bash
go install gitlab.com/nanmu42/rangelist/cmd/runner@latest
```

and it should be available if your `$PATH` is properly set up.