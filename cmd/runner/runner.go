package main

import "gitlab.com/nanmu42/rangelist"

func main() {
	rl := rangelist.RangeList{}

	noError(rl.Add([2]int{1, 5}))
	noError(rl.Print())
	// Should display: [1, 5)

	noError(rl.Add([2]int{10, 20}))
	noError(rl.Print())
	// Should display: [1, 5) [10, 20)

	noError(rl.Add([2]int{20, 20}))
	noError(rl.Print())
	// Should display: [1, 5) [10, 20)

	noError(rl.Add([2]int{20, 21}))
	noError(rl.Print())
	// Should display: [1, 5) [10, 21)

	noError(rl.Add([2]int{2, 4}))
	noError(rl.Print())
	// Should display: [1, 5) [10, 21)

	noError(rl.Add([2]int{3, 8}))
	noError(rl.Print())
	// Should display: [1, 8) [10, 21)

	noError(rl.Remove([2]int{10, 10}))
	noError(rl.Print())
	// Should display: [1, 8) [10, 21)

	noError(rl.Remove([2]int{10, 11}))
	noError(rl.Print())
	// Should display: [1, 8) [11, 21)

	noError(rl.Remove([2]int{15, 17}))
	noError(rl.Print())
	// Should display: [1, 8) [11, 15) [17, 21)

	noError(rl.Remove([2]int{3, 19}))
	noError(rl.Print())
	// Should display: [1, 3) [19, 21)
}

func noError(err error) {
	if err != nil {
		panic(err)
	}
}
