// Package rangelist implements RangeList, a list of ranges,
// which can add any new range and remove any specified range regardless its bound.
//
// RangeList needs at least Includes() or Contains() method to make itself useful,
// which is left as an exercise for the documentation reader(namely, you). (doge)
package rangelist

import (
	"fmt"
	"sort"
	"strings"
)

// RangeList is an aggregate of left-closed and right-open ranges.
// A pair of integers define a left-closed and right-open range, for example: [1, 5).
// This range includes integers: 1, 2, 3, and 4.
//
// A RangeList could be [1, 5), [10, 11), [100, 201).
//
// RangeList is not concurrent safe.
type RangeList struct {
	// always ascending sorted and no overlapping ranges
	pairs pairs
}

// Add a range into the RangeList.
//
//           0        1        2             3     4
// -∞        L-----R  L--R     L---------R   L-R   L-----R        +∞
//
// A RangeList consists of one or multiple non-overlapping ranges,
// and it has the following properties:
// 1. a range's right bound is greater than its left bound(obvious but easily overlooking).
// 2. range N+1's left bound is greater than range N's right bound.
// 3. if number v is less or equal than range N's left bound, it is less than range N+1's left bound.
//
// Using binary search, we obtain the smallest N at which v is less or equal than range N's left bound,
// and thus:
// N-1's left bound < v <= N's left bound
//
// Given a new range `[left, right)` to add, we make use of previous method to determine
// the smallest `lPos` and `rPos` of `left` and `right`, and then we further sequentially discuss:
// 1. if `left` is less or equal than range (lPos-1)'s right bound, if there's any;
// 2. if `left` is greater than any existed range's right bound;
// 3. if `right` equals to range (rPos)'s left if there's any;
// 4. if `right` is less than any existed range's left bound;
// 5. prepare the new range to insert, reconcile its left and right;
// 6. if the new range is a proper subset of the existed range at `lPos`;
// 7. if the new range overlaps any existed ranges.
func (l *RangeList) Add(rangeElement [2]int) error {
	if l.pairs == nil {
		l.pairs = make(pairs, 0, 8)
	}

	left, right := rangeElement[0], rangeElement[1]
	if left >= right {
		// empty range, relax
		return nil
	}

	var (
		lPos   = l.pairs.FirstLeftIndex(left)
		rPos   = l.pairs.FirstLeftIndex(right)
		length = len(l.pairs)
	)
	if lPos > 0 && l.pairs[lPos-1].Right >= left {
		lPos = lPos - 1
	} else if lPos == length {
		// easy scenario, add a range to the rightmost
		l.pairs.AddAt(length, pair{
			Left:  left,
			Right: right,
		})
		return nil
	}
	if rPos == length {
		// relax
	} else if l.pairs[rPos].Left == right {
		rPos = rPos + 1
	} else if rPos == 0 {
		// easy scenario, add a range to the leftmost
		l.pairs.AddAt(0, pair{
			Left:  left,
			Right: right,
		})
		return nil
	}

	newPair := pair{
		Left:  min(left, l.pairs[lPos].Left),
		Right: max(right, l.pairs[rPos-1].Right),
	}
	if lPos == rPos && newPair == l.pairs[lPos] {
		// adding a subset of an existed pair, abort
		return nil
	}

	// new pair without overlaps
	if lPos == rPos {
		l.pairs.AddAt(lPos, newPair)
		return nil
	}

	// use one existed slot
	l.pairs[lPos] = newPair
	if lPos+1 == rPos {
		return nil
	}

	// remove remaining overlapping pairs of our newcomer
	l.pairs.RemoveBetween(lPos+1, rPos)
	return nil
}

// Remove a range from the RangeList.
//
// See comment's on Add() for the big picture of implementation.
//
// After `lPos` and `rPos` shows up, we discuss sequentially:
// 1. if `right` is less than any range's left bound;
// 2. if `right` is less than range (rPos-1)'s right bound;
// 3. if our new range is a proper subset of one existed range;
// 4. if `left` is less than range (lPos-1)'s right bound;
// 5. if our new range overlaps more than one existed ranges.
func (l *RangeList) Remove(rangeElement [2]int) error {
	if len(l.pairs) == 0 {
		// nothing to lose, relax
		return nil
	}

	left, right := rangeElement[0], rangeElement[1]
	if left >= right {
		// empty range, relax
		return nil
	}

	var (
		lPos = l.pairs.FirstLeftIndex(left)
		rPos = l.pairs.FirstLeftIndex(right)
	)

	if rPos == 0 {
		// easy scenario, no range matched
		return nil
	}

	if l.pairs[rPos-1].Right > right {
		if lPos == rPos {
			// breaks into two parts
			l.pairs.AddAt(rPos, pair{
				Left:  right,
				Right: l.pairs[rPos-1].Right,
			})
			l.pairs[lPos-1].Right = left
			return nil
		}

		// reuse the slot
		l.pairs[rPos-1].Left = right
		rPos = rPos - 1
	}

	if lPos > 0 && l.pairs[lPos-1].Right > left {
		l.pairs[lPos-1].Right = left
	}

	if lPos == rPos {
		return nil
	}
	l.pairs.RemoveBetween(lPos, rPos)
	return nil
}

const emptyRangeListPrint = "<Empty>"

// String implements fmt.Stringer.
//
// The functionality of Print() is isolated for better observability during test.
func (l *RangeList) String() (s string) {
	if len(l.pairs) == 0 {
		// The test/spec does not specify what an empty RangeList prints like,
		// and it just feels lame to return an error.
		return emptyRangeListPrint
	}

	// based on common usage
	const typicalItemSize = len("[11, 135) ")

	var b strings.Builder
	b.Grow(len(l.pairs) * typicalItemSize)

	b.WriteString(l.pairs[0].String())
	for _, pair := range l.pairs[1:] {
		b.WriteString(" ")
		b.WriteString(pair.String())
	}

	return b.String()
}

// Print prints RangeList to stdout in [1, 5) [10, 20) style.
func (l *RangeList) Print() error {
	fmt.Println(l.String())
	return nil
}

type pairs []pair

// AddAt adds value at the index
func (p *pairs) AddAt(index int, value pair) {
	if len(*p) == index {
		*p = append(*p, value)
		return
	}

	*p = append((*p)[:index+1], (*p)[index:]...)
	(*p)[index] = value
}

// RemoveBetween removes [start, end) items in pairs by indexes
func (p *pairs) RemoveBetween(start, end int) {
	copy((*p)[start:], (*p)[end:])
	*p = (*p)[:len(*p)-(end-start)]
}

// FirstLeftIndex returns the first index where pair.Left >= min
func (p pairs) FirstLeftIndex(min int) (index int) {
	index = sort.Search(len(p), func(i int) bool {
		return p[i].Left >= min
	})

	return
}

type pair struct {
	// closed, must be less or equal than right
	Left int
	// open
	Right int
}

func (p pair) String() string {
	return fmt.Sprintf("[%d, %d)", p.Left, p.Right)
}

func min(a, b int) int {
	if a < b {
		return a
	}

	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}
