# RangeList

[![pipeline status](https://gitlab.com/nanmu42/rangelist/badges/master/pipeline.svg)](https://gitlab.com/nanmu42/rangelist/-/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/nanmu42/rangelist)](https://goreportcard.com/report/gitlab.com/nanmu42/rangelist)

Package rangelist implements RangeList, a list of ranges,
which can add any new range and remove any specified range regardless its bound.

RangeList needs at least Includes() or Contains() method to make itself useful,
which is left as an exercise for the documentation reader(namely, you). (doge)

## Development

* Go 1.17 or higher
* Configure your IDE with following file watcher(here using Goland as example):
  * `goimports -w $FilePath$`
  * `golangci-lint run --disable=deadcode,unused $FileDir$`

## Test

```bash
go test -v ./...
```

## Runner

The runner is provided as resolution to the specified question.

To install, run:

```bash
go install gitlab.com/nanmu42/rangelist/cmd/runner@latest
```

and it should be available if your `$PATH` is properly set up.

Or, if you like getting your hands dirty:

```bash
cd cmd/runner
go build
./runner
```
