package rangelist

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRangeList(t *testing.T) {
	var (
		err error
		s   string
		rl  RangeList
	)

	err = rl.Add([2]int{1, 5})
	require.NoError(t, err)
	s = rl.String()
	require.Equal(t, `[1, 5)`, s)

	err = rl.Add([2]int{10, 20})
	require.NoError(t, err)
	s = rl.String()
	require.Equal(t, `[1, 5) [10, 20)`, s)

	err = rl.Add([2]int{20, 20})
	require.NoError(t, err)
	s = rl.String()
	require.Equal(t, `[1, 5) [10, 20)`, s)

	err = rl.Add([2]int{20, 21})
	require.NoError(t, err)
	s = rl.String()
	require.Equal(t, `[1, 5) [10, 21)`, s)

	err = rl.Add([2]int{2, 4})
	require.NoError(t, err)
	s = rl.String()
	require.Equal(t, `[1, 5) [10, 21)`, s)

	err = rl.Add([2]int{3, 8})
	require.NoError(t, err)
	s = rl.String()
	require.Equal(t, `[1, 8) [10, 21)`, s)

	err = rl.Remove([2]int{10, 10})
	require.NoError(t, err)
	s = rl.String()
	require.Equal(t, `[1, 8) [10, 21)`, s)

	err = rl.Remove([2]int{10, 11})
	require.NoError(t, err)
	s = rl.String()
	require.Equal(t, `[1, 8) [11, 21)`, s)

	err = rl.Remove([2]int{15, 17})
	require.NoError(t, err)
	s = rl.String()
	require.Equal(t, `[1, 8) [11, 15) [17, 21)`, s)

	err = rl.Remove([2]int{3, 19})
	require.NoError(t, err)
	s = rl.String()
	require.Equal(t, `[1, 3) [19, 21)`, s)
}

func Test_pair_String(t *testing.T) {
	type fields struct {
		Left  int
		Right int
	}
	tests := []struct {
		fields fields
		want   string
	}{
		{
			fields: fields{},
			want:   "[0, 0)",
		},
		{
			fields: fields{
				Left:  -1,
				Right: 0,
			},
			want: "[-1, 0)",
		},
		{
			fields: fields{
				Left:  -123456789,
				Right: 1,
			},
			want: "[-123456789, 1)",
		},
		{
			fields: fields{
				Left:  -123456789,
				Right: 123456789,
			},
			want: "[-123456789, 123456789)",
		},
	}
	for _, tt := range tests {
		t.Run(tt.want, func(t *testing.T) {
			p := pair{
				Left:  tt.fields.Left,
				Right: tt.fields.Right,
			}
			if got := p.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRangeList_print(t *testing.T) {
	tests := []struct {
		name  string
		pairs []pair
		wantS string
	}{
		{
			name:  "empty",
			pairs: nil,
			wantS: emptyRangeListPrint,
		},
		{
			name: "one",
			pairs: []pair{
				{
					Left:  0,
					Right: 0,
				},
			},
			wantS: "[0, 0)",
		},
		{
			name: "one but long",
			pairs: []pair{
				{
					Left:  -1234567890,
					Right: 1234567890,
				},
			},
			wantS: "[-1234567890, 1234567890)",
		},
		{
			name: "two 1",
			pairs: []pair{
				{
					Left:  -3,
					Right: 5,
				},
				{
					Left:  6,
					Right: 10,
				},
			},
			wantS: "[-3, 5) [6, 10)",
		},
		{
			name: "two 2",
			pairs: []pair{
				{
					Left:  -3,
					Right: 5,
				},
				{
					Left:  66666,
					Right: 10000000,
				},
			},
			wantS: "[-3, 5) [66666, 10000000)",
		},
		{
			name: "three 1",
			pairs: []pair{
				{
					Left:  -300,
					Right: -200,
				},
				{
					Left:  -3,
					Right: 5,
				},
				{
					Left:  66666,
					Right: 10000000,
				},
			},
			wantS: "[-300, -200) [-3, 5) [66666, 10000000)",
		},
		{
			name: "three 2",
			pairs: []pair{
				{
					Left:  -999999999,
					Right: -2000,
				},
				{
					Left:  -300,
					Right: -200,
				},
				{
					Left:  -3,
					Right: 5,
				},
				{
					Left:  66666,
					Right: 10000000,
				},
			},
			wantS: "[-999999999, -2000) [-300, -200) [-3, 5) [66666, 10000000)",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &RangeList{
				pairs: tt.pairs,
			}
			gotS := l.String()
			if gotS != tt.wantS {
				t.Errorf("String() gotS = %v, want %v", gotS, tt.wantS)
			}
		})
	}
}

func Test_pairs_AddAt(t *testing.T) {
	p := make(pairs, 0, 8)

	p.AddAt(0, pair{
		Left:  0,
		Right: 0,
	})
	p.AddAt(0, pair{
		Left:  1,
		Right: 1,
	})
	p.AddAt(0, pair{
		Left:  2,
		Right: 2,
	})
	p.AddAt(1, pair{
		Left:  111,
		Right: 111,
	})
	p.AddAt(3, pair{
		Left:  333,
		Right: 333,
	})
	p.AddAt(5, pair{
		Left:  555,
		Right: 555,
	})

	require.Equal(t, pairs{
		{
			Left:  2,
			Right: 2,
		},
		{
			Left:  111,
			Right: 111,
		},
		{
			Left:  1,
			Right: 1,
		},
		{
			Left:  333,
			Right: 333,
		},
		{
			Left:  0,
			Right: 0,
		},
		{
			Left:  555,
			Right: 555,
		},
	}, p)
}

func Test_pairs_RemoveBetween(t *testing.T) {
	p := pairs{
		{
			Left:  2,
			Right: 2,
		},
		{
			Left:  111,
			Right: 111,
		},
		{
			Left:  1,
			Right: 1,
		},
		{
			Left:  333,
			Right: 333,
		},
		{
			Left:  0,
			Right: 0,
		},
		{
			Left:  555,
			Right: 555,
		},
	}

	p.RemoveBetween(4, len(p))
	require.Equal(t, pairs{
		{
			Left:  2,
			Right: 2,
		},
		{
			Left:  111,
			Right: 111,
		},
		{
			Left:  1,
			Right: 1,
		},
		{
			Left:  333,
			Right: 333,
		},
	}, p)

	p.RemoveBetween(0, 2)
	require.Equal(t, pairs{
		{
			Left:  1,
			Right: 1,
		},
		{
			Left:  333,
			Right: 333,
		},
	}, p)

	p.RemoveBetween(0, 2)
	require.Equal(t, pairs{}, p)
}

func Test_pairs_FirstLeftIndex(t *testing.T) {
	p := pairs{
		{
			Left:  1,
			Right: 5,
		},
		{
			Left:  7,
			Right: 9,
		},
		{
			Left:  11,
			Right: 15,
		},
	}

	tests := []struct {
		name      string
		min       int
		wantIndex int
	}{
		{
			name:      "small",
			min:       -1,
			wantIndex: 0,
		},
		{
			name:      "first edge",
			min:       1,
			wantIndex: 0,
		},
		{
			name:      "first edge 2",
			min:       5,
			wantIndex: 1,
		},
		{
			name:      "second edge 1",
			min:       7,
			wantIndex: 1,
		},
		{
			name:      "second",
			min:       8,
			wantIndex: 2,
		},
		{
			name:      "third edge",
			min:       11,
			wantIndex: 2,
		},
		{
			name:      "fourth",
			min:       12,
			wantIndex: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotIndex := p.FirstLeftIndex(tt.min); gotIndex != tt.wantIndex {
				t.Errorf("FirstLeftIndex() = %v, want %v", gotIndex, tt.wantIndex)
			}
		})
	}
}

func TestRangeList_Add(t *testing.T) {
	sample := func() RangeList {
		return RangeList{pairs: pairs{
			{
				Left:  -5,
				Right: 1,
			},
			{
				Left:  6,
				Right: 9,
			},
			{
				Left:  12,
				Right: 19,
			},
			{
				Left:  101,
				Right: 2000,
			},
		}}
	}

	tests := []struct {
		name         string
		rangeList    RangeList
		rangeElement [2]int
		wantPrint    string
		wantErr      bool
	}{
		{
			name:         "empty origin",
			rangeList:    RangeList{},
			rangeElement: [2]int{1, 2},
			wantPrint:    "[1, 2)",
		},
		{
			name:         "leftmost",
			rangeList:    sample(),
			rangeElement: [2]int{-100, -50},
			wantPrint:    "[-100, -50) [-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "left edge",
			rangeList:    sample(),
			rangeElement: [2]int{-100, -5},
			wantPrint:    "[-100, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "left edge 2",
			rangeList:    sample(),
			rangeElement: [2]int{2, 6},
			wantPrint:    "[-5, 1) [2, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "left overlap",
			rangeList:    sample(),
			rangeElement: [2]int{-100, -3},
			wantPrint:    "[-100, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "overlap 1",
			rangeList:    sample(),
			rangeElement: [2]int{-100, 3},
			wantPrint:    "[-100, 3) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "overlap to edge",
			rangeList:    sample(),
			rangeElement: [2]int{-100, 6},
			wantPrint:    "[-100, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "overlap 2",
			rangeList:    sample(),
			rangeElement: [2]int{-100, 8},
			wantPrint:    "[-100, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "overlap 3",
			rangeList:    sample(),
			rangeElement: [2]int{-100, 15},
			wantPrint:    "[-100, 19) [101, 2000)",
		},
		{
			name:         "overlap 4",
			rangeList:    sample(),
			rangeElement: [2]int{-100, 1500},
			wantPrint:    "[-100, 2000)",
		},
		{
			name:         "overlap two ranges",
			rangeList:    sample(),
			rangeElement: [2]int{7, 15},
			wantPrint:    "[-5, 1) [6, 19) [101, 2000)",
		},
		{
			name:         "overlap two ranges on edges",
			rangeList:    sample(),
			rangeElement: [2]int{6, 19},
			wantPrint:    "[-5, 1) [6, 19) [101, 2000)",
		},
		{
			name:         "overlap many ranges",
			rangeList:    sample(),
			rangeElement: [2]int{-1000, 20000},
			wantPrint:    "[-1000, 20000)",
		},
		{
			name:         "overlap many ranges 2",
			rangeList:    sample(),
			rangeElement: [2]int{0, 20000},
			wantPrint:    "[-5, 20000)",
		},
		{
			name:         "overlap many ranges 3",
			rangeList:    sample(),
			rangeElement: [2]int{0, 1995},
			wantPrint:    "[-5, 2000)",
		},
		{
			name:         "invalid",
			rangeList:    sample(),
			rangeElement: [2]int{-6, -6},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "invalid 2",
			rangeList:    sample(),
			rangeElement: [2]int{6, 6},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "invalid 3",
			rangeList:    sample(),
			rangeElement: [2]int{9, 6},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "middle insert",
			rangeList:    sample(),
			rangeElement: [2]int{10, 11},
			wantPrint:    "[-5, 1) [6, 9) [10, 11) [12, 19) [101, 2000)",
		},
		{
			name:         "rightmost",
			rangeList:    sample(),
			rangeElement: [2]int{3000, 5000},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000) [3000, 5000)",
		},
		{
			name:         "right to the edge",
			rangeList:    sample(),
			rangeElement: [2]int{2000, 2500},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2500)",
		},
		{
			name:         "right to the edge2",
			rangeList:    sample(),
			rangeElement: [2]int{19, 50},
			wantPrint:    "[-5, 1) [6, 9) [12, 50) [101, 2000)",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.rangeList.Add(tt.rangeElement); (err != nil) != tt.wantErr {
				t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
			}
			s := tt.rangeList.String()
			require.Equal(t, tt.wantPrint, s)
		})
	}
}

func TestRangeList_Remove(t *testing.T) {
	sample := func() RangeList {
		return RangeList{pairs: pairs{
			{
				Left:  -5,
				Right: 1,
			},
			{
				Left:  6,
				Right: 9,
			},
			{
				Left:  12,
				Right: 19,
			},
			{
				Left:  101,
				Right: 2000,
			},
		}}
	}

	tests := []struct {
		name         string
		rangeList    RangeList
		rangeElement [2]int
		wantPrint    string
		wantErr      bool
	}{
		{
			name:         "empty origin",
			rangeList:    RangeList{},
			rangeElement: [2]int{1, 2},
			wantPrint:    emptyRangeListPrint,
		},
		{
			name:         "out of range - left",
			rangeList:    sample(),
			rangeElement: [2]int{-100, -50},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "out of range - right",
			rangeList:    sample(),
			rangeElement: [2]int{2001, 5000},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "edge 1",
			rangeList:    sample(),
			rangeElement: [2]int{-100, -5},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "edge 2",
			rangeList:    sample(),
			rangeElement: [2]int{1, 6},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "edge 3",
			rangeList:    sample(),
			rangeElement: [2]int{2, 6},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "edge 4",
			rangeList:    sample(),
			rangeElement: [2]int{19, 101},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "edge 5",
			rangeList:    sample(),
			rangeElement: [2]int{20, 101},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "edge 6",
			rangeList:    sample(),
			rangeElement: [2]int{2000, 2001},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "invalid 1",
			rangeList:    sample(),
			rangeElement: [2]int{17, 17},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "invalid 2",
			rangeList:    sample(),
			rangeElement: [2]int{20, 17},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "left overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-10, -4},
			wantPrint:    "[-4, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "left overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-10, 1},
			wantPrint:    "[6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "left overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-10, 6},
			wantPrint:    "[6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "left overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-10, 8},
			wantPrint:    "[8, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "left overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-10, 12},
			wantPrint:    "[12, 19) [101, 2000)",
		},
		{
			name:         "left overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-10, 16},
			wantPrint:    "[16, 19) [101, 2000)",
		},
		{
			name:         "overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-3, 4},
			wantPrint:    "[-5, -3) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-3, 7},
			wantPrint:    "[-5, -3) [7, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-3, 9},
			wantPrint:    "[-5, -3) [12, 19) [101, 2000)",
		},
		{
			name:         "overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-3, 12},
			wantPrint:    "[-5, -3) [12, 19) [101, 2000)",
		},
		{
			name:         "overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-3, 14},
			wantPrint:    "[-5, -3) [14, 19) [101, 2000)",
		},
		{
			name:         "overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-3, 1999},
			wantPrint:    "[-5, -3) [1999, 2000)",
		},
		{
			name:         "overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-3, 2000},
			wantPrint:    "[-5, -3)",
		},
		{
			name:         "overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{-3, 2001},
			wantPrint:    "[-5, -3)",
		},
		{
			name:         "whole",
			rangeList:    sample(),
			rangeElement: [2]int{-5, 2001},
			wantPrint:    emptyRangeListPrint,
		},
		{
			name:         "whole",
			rangeList:    sample(),
			rangeElement: [2]int{-6, 2001},
			wantPrint:    emptyRangeListPrint,
		},
		{
			name:         "right overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{150, 2300},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 150)",
		},
		{
			name:         "right overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{101, 2300},
			wantPrint:    "[-5, 1) [6, 9) [12, 19)",
		},
		{
			name:         "right overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{19, 2300},
			wantPrint:    "[-5, 1) [6, 9) [12, 19)",
		},
		{
			name:         "right overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{18, 2300},
			wantPrint:    "[-5, 1) [6, 9) [12, 18)",
		},
		{
			name:         "right overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{7, 2300},
			wantPrint:    "[-5, 1) [6, 7)",
		},
		{
			name:         "right overlapping",
			rangeList:    sample(),
			rangeElement: [2]int{15, 140},
			wantPrint:    "[-5, 1) [6, 9) [12, 15) [140, 2000)",
		},
		{
			name:         "break",
			rangeList:    sample(),
			rangeElement: [2]int{-4, 0},
			wantPrint:    "[-5, -4) [0, 1) [6, 9) [12, 19) [101, 2000)",
		},
		{
			name:         "break",
			rangeList:    sample(),
			rangeElement: [2]int{150, 151},
			wantPrint:    "[-5, 1) [6, 9) [12, 19) [101, 150) [151, 2000)",
		},
		{
			name:         "break",
			rangeList:    sample(),
			rangeElement: [2]int{13, 17},
			wantPrint:    "[-5, 1) [6, 9) [12, 13) [17, 19) [101, 2000)",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.rangeList.Remove(tt.rangeElement); (err != nil) != tt.wantErr {
				t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
			}
			s := tt.rangeList.String()
			require.Equal(t, tt.wantPrint, s)
		})
	}
}
